import json
from collections import Counter

# open data/galaxy_metadata.json
with open('data/galaxy_metadata.json', 'r') as meta_file:
    repositories_metadata = json.load(meta_file)

# print the first entry
#pretty_str = json.dumps(repositories_metadata[2], indent=4)
#print(pretty_str)


def  distribution_n_keys():
    # Step 1: Count the number of keys in each dictionary
    keys_count = [len(d) for d in repositories_metadata]

    # Step 2: Compute the distribution of key counts
    distribution = Counter(keys_count)

    # Print the distribution
    print("Distribution of Number of Keys:")
    for key_count, frequency in distribution.items():
        print(f"{key_count} keys: {frequency} time(s)")


def latest_revision():
    tool = repositories_metadata[2]
    latest_revision_id = max(iter(tool.keys()))
    print(F"Latest revision is: {latest_revision_id}")
    latest_revision = tool[latest_revision_id]

    # print the first entry
    pretty_str = json.dumps(latest_revision, indent=4)
    print(pretty_str)

# distribution of humber of tools in each latest revision
N_tools = []
for repo in repositories_metadata:
    latest_revision_id = max(iter(repo.keys()))
    latest_revision = repo[latest_revision_id]
    if 'tools' in latest_revision.keys():
        N_tools.append(len(latest_revision['tools']))

distribution = Counter(N_tools)
print("Distribution of Number of Tools in Latest Revision:")
for n_tools, frequency in distribution.items():
    print(f"{n_tools} tools: {frequency} time(s)")

