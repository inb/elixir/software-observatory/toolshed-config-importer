from repos_config_importer import toolShedMetadataFetcher, repositoryKeys, shed_repository

class Test_ShedRepsitory:

    def test_valid_url(self):
        repository_id = "783bde422b425bd9"
        changeset_revision = "f71fd828c126"    
        keys = repositoryKeys(repository_id, changeset_revision)
        repository = shed_repository(keys)
        toolShedMDF = toolShedMetadataFetcher( only_new=False)
        toolShedMDF.repositories = [repository]
        repo = toolShedMDF.repositories[0]
        repo.parse_valid_XMLs()    
        
        print(repo.metadata)
        assert repo.metadata[0]['data']['id'] == '10x_bamtofastq'

        