from bs4 import BeautifulSoup

from repos_config_importer import valid_XML
class Test_ParseXmls:
    def test_parse_valid_XMLs(self):
        xml_string = """
        <tool id="bamtools_split_mapped" name="Split BAM by reads mapping status" version="@TOOL_VERSION@+galaxy@VERSION_SUFFIX@">
            <description>into a mapped and an unmapped dataset</description>
            <macros>
                <import>macros.xml</import>
            </macros>
            <expand macro="requirements" />
            <command>
                <![CDATA[
                    ln -s '${input_bam}' 'localbam.bam' &&
                    ln -s '${input_bam.metadata.bam_index}' 'localbam.bam.bai' &&
                    bamtools split -mapped
                    -in localbam.bam
                    -stub split_bam
                ]]>
            </command>
            <inputs>
                <param name="input_bam" type="data" format="bam" label="BAM dataset to split by mapped/unmapped"/>
            </inputs>
            <outputs>
                <data format="bam" name="mapped" label="${input_bam.name} mapped" from_work_dir="split_bam.MAPPED.bam" />
                <data format="bam" name="unmapped" label="${input_bam.name} unmapped" from_work_dir="split_bam.UNMAPPED.bam" />
            </outputs>
            <tests>
                <test>
                    <param name="input_bam" ftype="bam" value="bamtools-input1.bam"/>
                    <output name="mapped" file="split_bam.MAPPED.bam"  compare="sim_size" delta="200" />
                    <output name="unmapped" file="split_bam.UNMAPPED.bam"  compare="sim_size" delta="200" />
                </test>
            </tests>
            <help>
        **What is does**

        BAMTools split is a utility for splitting BAM files. It is based on BAMtools suite of tools by Derek Barnett (https://github.com/pezmaster31/bamtools).

        -----

        **How it works**

        Splits the input BAM file into 2 output files containing mapped and unmapped reads, respectively.

        -----

        .. class:: infomark

        **More information**

        Additional information about BAMtools can be found at https://github.com/pezmaster31/bamtools/wiki

            </help>
            <citations>
                <citation type="doi">10.1093/bioinformatics/btr174</citation>
            </citations>
        </tool>
        """


        xml_macros = """
            <?xml version="1.0"?>
            <macros>
                <token name="@TOOL_VERSION@">2.5.2</token>
                <token name="@VERSION_SUFFIX@">1</token>
                <xml name="requirements">
                    <requirements>
                        <requirement type="package" version="@TOOL_VERSION@">bamtools</requirement>
                        <yield />
                    </requirements>
                </xml>
                <xml name="citations">
                    <citations>
                        <citation type="doi">10.1093/bioinformatics/btr174</citation>
                        <citation type="doi">10.1093/bioinformatics/foo</citation>
                    </citations>
                </xml>
            </macros>
        """
        
        repo = valid_XML(BeautifulSoup(xml_string, features="xml"), 'tool_path')
        repo.imports = BeautifulSoup(xml_macros, features="xml")
        repo.get_tokens()
        repo.build_metadata()

        assert repo.metadata['id'] == 'bamtools_split_mapped'
        assert repo.metadata['name'] == 'Split BAM by reads mapping status'
        assert repo.metadata['version'] == '2.5.2+galaxy1'
        assert repo.metadata['description'] == 'into a mapped and an unmapped dataset'
        assert repo.metadata['requirements'] == [{'type': 'package', 'version': '2.5.2', 'name': 'bamtools'}]
        assert repo.metadata['citation'] == [{'type': 'doi', 'value': '10.1093/bioinformatics/btr174'}, {'type': 'doi', 'value': '10.1093/bioinformatics/foo'}]
        assert repo.metadata['command'] == 'ln -s \'${input_bam}\' \'localbam.bam\' &&\n                    ln -s \'${input_bam.metadata.bam_index}\' \'localbam.bam.bai\' &&\n                    bamtools split -mapped\n                    -in localbam.bam\n                    -stub split_bam'
        assert repo.metadata['input'] == ['bam']
        assert repo.metadata['output'] == ['bam']

    def test_parse_missing_fields_XML(self):
        """
        Test that the parser can handle missing fields in the XML
        """
        xml_tool = """
        <tool id="bamtools_split_mapped" name="Split BAM by reads mapping status" version="@TOOL_VERSION@+galaxy@VERSION_SUFFIX@">
            <description>into a mapped and an unmapped dataset</description>
            <macros>
                <import>macros.xml</import>
            </macros>
            <expand macro="requirements" />
            <inputs>
            </inputs>
            <tests>
                <test>
                    <param name="input_bam" ftype="bam" value="bamtools-input1.bam"/>
                    <output name="mapped" file="split_bam.MAPPED.bam"  compare="sim_size" delta="200" />
                    <output name="unmapped" file="split_bam.UNMAPPED.bam"  compare="sim_size" delta="200" />
                </test>
            </tests>
        </tool>
        """

        xml_macros = """
            <?xml version="1.0"?>
            <macros>
                <token name="@TOOL_VERSION@">2.5.2</token>
                <token name="@VERSION_SUFFIX@">1</token>
                <xml name="requirements">
                    <requirements>
                        <requirement type="package" version="@TOOL_VERSION@">bamtools</requirement>
                        <yield />
                    </requirements>
                </xml>
                <xml name="citations">
                    <citations>
                    </citations>
                </xml>
            </macros>
        """

        repo = valid_XML(BeautifulSoup(xml_tool, features="xml"), 'tool_path')
        repo.imports = BeautifulSoup(xml_macros, features="xml")
        repo.get_tokens()
        repo.build_metadata()

        assert repo.metadata['id'] == 'bamtools_split_mapped'


        




