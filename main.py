import logging
import argparse
import sys

from dotenv import load_dotenv

from repos_config_importer import toolShedMetadataFetcher


def import_data(only_new):
    try:

        # ---------- Preparation ----------

        # 0.1 Set up logging
        parser = argparse.ArgumentParser(
            description="Importer of OpenEBench tools from OpenEBench Tool API"
        )
        parser.add_argument(
            "--loglevel", "-l",
            help=("Set the logging level"),
            default="INFO",
        )

        args = parser.parse_args()
        numeric_level = getattr(logging, args.loglevel.upper())
        print(f"Logging level: {numeric_level}")

        logging.basicConfig(level=numeric_level, format='%(asctime)s - %(levelname)s - toolshed - %(message)s', stream=sys.stdout)        
        logging.getLogger('urllib3').setLevel(logging.INFO)


        # 0.2 Load .env
        load_dotenv()
        logging.info("state_importation - 1")

        # ---------- Importation --------------

        # Download and process configuration files in repos
        logging.debug('Initializing toolShedMetadataFetcher object to fetch metadata from the Galaxy Toolshed')
        toolShedMDF = toolShedMetadataFetcher(only_new=only_new)

        logging.debug('Getting repositories from metadata in database')
        toolShedMDF.repositories = toolShedMDF.get_repositories()
        
        logging.debug('Getting repositories zips and processing.')
        toolShedMDF.get_toolShed_files()


    except Exception as e:
        logging.exception("Exception occurred")
        logging.error(f"error - {type(e).__name__} - {e}")
        logging.info("state_importation - 2")
        exit(1)
    
    else: 
        logging.info("state_importation - 0")
        exit(0)


if __name__ == '__main__':
    import_data(False) 

