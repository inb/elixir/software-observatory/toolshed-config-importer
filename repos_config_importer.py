
import requests
import json
import zipfile
import io
import os
from bs4 import BeautifulSoup
from bs4.element import ProcessingInstruction
from typing import List
import logging

from utils import connect_db, push_entry, add_metadata_to_entry, clean_date_field


class toolShedMetadataFetcher():

    def __init__(self, only_new):
        self.only_new = only_new
        logging.debug(f"Only new:{self.only_new}")
        if self.only_new:
            # only unseen: only repos not already in DB are processed
            self.seen_repos = self.compute_initial_seen_repos()
        else:
            self.seen_repos = set()

        self.repositories = []
        self.all_metadata = []


    def compute_initial_seen_repos(self):
        seen = set()
        for entry in self.alambique.find({'@data_source':'toolshed'}):
            seen.add(entry['@source_url'])
        return(seen)
    
    def get_repositories(self):
        '''
        Builds a list of repositories from the repositories metadata extracted from the galaxy toolshed, which is 
        stored in alambique, with "@data-source" equal to "galaxy_metadata". 
        '''
        alambique = connect_db('alambique')
        repositories = []
        repos_n = 0
        repos_unseen = 0
        logging.debug('Fetching repo information from alambique')
        for entry in alambique.find({'@data_source':'galaxy_metadata'}):
            if entry.get('data'):
                repos_n += 1
                keys = repositoryKeys(entry['data']['repository_id'], entry['data']['changeset_revision'])
                if self.only_new:
                    if self.seen_repository(keys) == False:
                        repositories.append(shed_repository(keys))
                    else:
                        repos_unseen += 1
                else:
                    repositories.append(shed_repository(keys))
    
        logging.debug('number of repositories: %d'%(repos_n))
        logging.debug('number of repositories: %d'%(len(repositories)))

        return(repositories)
        
    def seen_repository(self, keys):
        ID_URL = f"https://toolshed.g2.bx.psu.edu/repository/download?repository_id={keys.repository_id}&changeset_revision={keys.changeset_revision}&file_type=zip"
        if ID_URL in self.seen_repos:
            return(True)
        else:
            return(False)
    
    def get_toolShed_files(self):
        for repo in self.repositories:
            repo.parse_valid_XMLs() ## XML downloading and parsing
            self.push_to_DB(repo.metadata)
            #self.all_metadata.append(repo.meta)
        
    def export_metadata(self, outfile):
        with open(outfile, 'w') as outf:
            json.dump(self.all_metadata, outf)

    def  push_to_DB(self, repo):
        '''
        Completes entry metadata and pushes it to the database
        '''
        alambique = connect_db('alambique')
        
        for entry in repo:
            identifier = f"galaxy_config/{entry['data']['id']}/cmd/{entry['data'].get('version')}"
            entry = clean_date_field(entry)
            document_w_metadata = add_metadata_to_entry(identifier, entry, alambique)
            push_entry(document_w_metadata, alambique)
                
class repositoryKeys():
    def __init__(self, repository_id, changeset_revision):
        self.repository_id = repository_id
        self.changeset_revision = changeset_revision

def get_url(url):
    session = requests.Session()
    try:
        re = session.get(url, timeout=30)
    except Exception as e:
        logging.warning(f'error - {type(e).__name__} - {e}')
        return(None)
    else:    
        return(re)

class valid_XML():
    '''
    This class is used to store the valid XMLs from the toolshed repository.
    
    '''
    def __init__(self, tool, path):
        self.tool = tool
        self.path = path
        self.imports = [] # list of BeautifulSoup objects
        self.tokens = {} # dictionary of tokens
        self.metadata = {}
    

    def get_tokens(self):
        """
        Gets the tokens from the XMLs in the instance. The tokens can be in
        the imports or in the tool file content

        Returns:
            dict: A dictionary containing the tokens extracted from the XML file.
        """
        tokens = {}
        # Retrieving tokens from imports
        for imp in self.imports:
            if isinstance(imp, ProcessingInstruction):
                continue
            tokens.update(self._parse_tokens(imp, tokens))
        
        # Retrieving tokens from the tool file
        tokens.update(self._parse_tokens(self.tool, tokens))

        self.tokens = tokens

    def _parse_tokens(self, BSmacros, tokens):
        """
        Parses the tokens from the given BeautifulSoup object and adds them to the tokens dictionary.

        Parameters:
        - BSmacros (BeautifulSoup): The BeautifulSoup object containing the macros.
        - tokens (dict): The dictionary to which the tokens will be added.

        Returns:
        - dict: The updated tokens dictionary.

        """
        fields = [a for a in BSmacros.findAll("token")]
        for e in fields:
            if '\\$' not in e.get_text().lstrip():
                tokens[e['name']] = e.get_text().lstrip()
        return(tokens)
    
    def replace_tokens(self, element, replacements):
        # Replace in the text of the current element
        if element.string and any(placeholder in element.string for placeholder in replacements):
            for placeholder, replacement in replacements.items():
                element.string = element.string.replace(placeholder, replacement)
        
        # Replace in the attributes of the current element
        for attr in element.attrs:
            if any(placeholder in element[attr] for placeholder in replacements):
                for placeholder, replacement in replacements.items():
                    element[attr] = element[attr].replace(placeholder, replacement)
        
        # Recursively replace in child elements
        for child in element.children:
            if child.name is not None:  # This checks if the child is a tag
                self.replace_tokens(child, replacements)
            
    def build_metadata(self):
        self.replace_tokens(self.tool, self.tokens)

        if self.imports:
            for imp in self.imports:
                if isinstance(imp, ProcessingInstruction):
                    continue
                self.replace_tokens(imp, self.tokens)
        
        self.metadata['id'] = self.tool.tool.get('id', None)
        self.metadata['name'] = self.tool.tool.get('name', None)
        self.metadata['version'] = self.tool.tool.get('version', None)
        self.metadata['description'] = self.tool.tool.description.get_text() if self.tool.tool.description else None
        self.metadata['code_file'] = self.tool.tool.code['file'] if self.tool.tool.code else None
        self.metadata['language'] = self.metadata['code_file'].split('.')[-1] if self.metadata['code_file'] else None
        if self.tool.tool.command:
            self.metadata['command'] = self.tool.tool.command.get_text().strip()
            if  'interpreter' in self.tool.tool.command.attrs.keys():
                self.metadata['interpreter'] = self.tool.tool.command['interpreter']
            else:
                self.metadata['interpreter'] = None
        else:
            self.metadata['command'] = None

        self.metadata['input'] =  self._parse_input()
        self.metadata['output'] = self._parse_output()
        self.metadata['help'] = str(self.tool.tool.help.get_text()) if self.tool.tool.help else None
        self.metadata['tests'] = self._existTest(self.tool)
        self.metadata['citation'] = self.extract_citations()
        self.metadata['requirements'] = self.extract_requirements()
         

    def extract_citations(self):
        '''
        Example of citations in the XML:
        <macros>
            <xml name="citations">
                <citations>
                    <citation type="doi">10.1093/bioinformatics/btr174</citation>
                    <citation type="doi">10.1093/bioinformatics/foo</citation>
                </citations>
            </xml>
        </macros>
        
        '''
        # a list that constins tool ans imports at the same level
        sources = self.imports
        sources.append(self.tool)
        for source in sources:
            if isinstance(source, ProcessingInstruction):
                continue
            
            if source.find('citations'):
                citations = source.find('citations')
                # Extract and return all citation DOIs
                doi_list = [{'type': citation['type'], 'value': citation.text} for citation in citations.findAll('citation')]
                return doi_list
    
        return None
    
    def extract_requirements(self):
        '''
        Example of requirements in the XML:
        <macros>
            <xml name="requirements">
                <requirements>
                    <requirement type="package" version="@TOOL_VERSION@">bamtools</requirement>

                </requirements>
            </xml>
        </macros>
        
        '''
        sources = self.imports
        sources.append(self.tool)
        for source in sources:
            if isinstance(source, ProcessingInstruction):
                continue

            if source.find('requirements'):
                requirements = source.find('requirements')
                # Extract and return all citation DOIs
                list = [{'type': req.get('type', None), 'version': req.get('version', None), 'name': req.text} for req in requirements.findAll('requirement')]
                return list
        
        return None

    
    def _parse_input(self):
        '''
        Example of input in the XML:
        <tool>
            <inputs>
                <param name="length" type="integer" value="10" label="Sequence length"/>
                <param name="pattern" type="text" value="ATGC" label="Sequence pattern"/>
                <param name="input" type="data" format="fasta" label="Input FASTA"/>
            </inputs>
        </tool>
        '''
        formats = set()
        if self.tool.find('inputs'):
            for param in self.tool.find('inputs').findAll('param'):
                param_type = param.get('type')
                if param_type == "data":
                    formats.add(param.get('format'))
                else:
                    pass
        
        return list(formats)
    
    def _parse_output(self):
        '''
        Example of output in the XML:
        <tool id="example_tool" name="Example Tool">
            <outputs>
                <data name="output1" format="txt" label="Output Text File"/>
                <data name="output2" format="fasta" label="Output FASTA File"/>
            </outputs>
        </tool>
        '''
        formats = set()
        if self.tool.find('outputs'):
            for data in self.tool.find('outputs').findAll('data'):
                formats.add(data.get('format'))

        return(list(formats))
        
    
    def _existTest(self, tool):
        if tool.findAll("test"):
            return(True)
        else:
            return(False)

class shed_repository(object):
    """
    This class is used to fetch the metadata from a toolshed repository.
    It uses the repository_id and the changeset_revision.
    Important: This class is not used for the parsing of the XMLs, but for the fetching of the metadata.
    """
    def __init__(self, keys):
        self.keys = keys
        self.z = None
        self.contentList=[]
        self.validXMLs = []
        self.metadata = []
    
    def parse_valid_XMLs(self): 
        self._get_repository_content()
        self.validXMLs = self._get_validXML()
        for tool in self.validXMLs:
            # --- parsing metadata from xmls -------
            tool.imports = self.get_imported_from_file(tool)
            tool.get_tokens()
            tool.build_metadata()
            # --- metadata from files -------
            tool.metadata['documentation'] = self.extract_from_files(tool.path)
            # --- other metadata -------
            entry = {
                'data': tool.metadata,
                '@repository_id': self.keys.repository_id,
                '@changeset_revision': self.keys.changeset_revision,
                '@tool_path': tool.path,
                '@data_source': 'toolshed'
                }

            self.metadata.append(entry)

    def _get_repository_content(self):
        fileurl = "https://toolshed.g2.bx.psu.edu/repository/download?repository_id={repository_id}&changeset_revision={changeset_revision}&file_type=zip"
        self.url = fileurl.format(repository_id=self.keys.repository_id,  
                            changeset_revision=self.keys.changeset_revision) 
        response = get_url(self.url)
        if response:
            try:
                self.z = zipfile.ZipFile(io.BytesIO(response.content), 'r')
                self.contentList = self.z.namelist()
            except Exception as e:
                logging.warning(f'error - {type(e).__name__} - {e}')
            
    def _get_validXML(self):
        results = []
        for f in self.contentList:
            if '.xml' in f and True not in [word in f for word in exclude]:
                fil = self.z.open(f)
                BS = BeautifulSoup(fil, features="xml")
                if self.thisIsATool(BS) == True:
                    valid_xml = valid_XML( 
                        tool = BS,
                        path = '/'.join(f.split('/')[:-1])
                    )
                    results.append(valid_xml)

        return(results)       

    def thisIsATool(self, BS):
        if BS.findAll('tool'):
            return(True)
        else:
            return(False)
 

    def get_imported_from_file(self, tool):
        imported = []
        if tool.tool.macros:
            imports = [a.get_text() for a in tool.tool.macros.findAll("import")]
            for imp in imports:
                # read the file and parse it
                filepath = tool.path + '/' + imp
                Import = self.z.open(filepath)
                BSimported = BeautifulSoup(Import, features="xml")
                imported.append(BSimported)
            
        return(imported)
    
    def get_file(self, filename, path):
        file = self.z.open(filename)
        return(file)
    
    def extract_from_files(self, path):
        '''
        Extracts the content of the files in the repository that are relevant for the metadata.
        '''
        docs = []
        for f in self.contentList:
            if 'readme' in f.lower():
                # extract text from file
                readme = self.get_file(f, path)
                docs.append({
                    'type': 'readme',
                    'content': readme.read().decode('utf-8', 'ignore'),
                    'url': None
                })
            
            elif 'install' in f.lower():
                # extract text from file
                install = self.get_file(f, path)
                    
                docs.append({
                    'type': 'installation instructions',
                    'content': install.read().decode('utf-8', 'ignore'),
                    'url': None
                })
            
            elif 'license' in f.lower():
                # extract text from file
                license = self.get_file(f, path)
                    
                docs.append({
                    'type': 'license',
                    'content': license.read().decode('utf-8', 'ignore'),
                    'url': None
                })
                
        return([])
        


exclude = ['dependencies','dependency','macros.xml','build.xml']

if __name__ == '__main__':
    # The repos metadata from galaxy contain the needed identifiers
    print('READING METADATA FOR IDS')
    GALAXY_METADATA=os.getenv('GALAXY_METADATA', 'galaxy_metadata.json')
    with open(GALAXY_METADATA, 'r') as outfile:
        repositories_metadata = json.load(outfile)
    toolShedMDF = toolShedMetadataFetcher(repositories_metadata)

    print('\nSARTING METADATA ZIP DOWNLOAD AND METADATA FETCHING')
    toolShedMDF.get_toolShed_files()
    
    print('\nExporting metadata as toolshed.json')
    toolShedMDF.export_metadata('toolshed.json')
    
    print('\nLoading to database')
    toolShedMDF.load_to_DB()
